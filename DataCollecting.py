def main():
    collect("GreaterG", "LesserG")

def collect(f1, f2):
    run1 = open("./Data/" + f1 +".txt", "r")
    run2 = open("./Data/" + f2 +".txt", "r")
    g1 = {}
    g2 = {}

    count = 1
    for line in run1:
        g1[count] = line
        count += 1

    count = 1
    for line in run2:
        g2[count] = line
        count += 1

    f = open("./Data/" + f1 + " Vs " + f2 +".txt", "w+")
    highSum = 0
    lowSum = 0
    for i in range(1, count):
        num = str(i)
        if i < 10:
            num = " " + num

        f.write("Maze " + num + ": ")
        f.write("\t" + f1 + " - " + g1[i])
        f.write("\t\t" + f2 + " - " + g2[i] + "\n")
        highSum += int(g1[i])
        lowSum += int(g2[i])

    f.write("\nAverages:\t" + f1 + " - " + str(highSum/50))
    f.write("\t" + f2 + " - " + str(lowSum/50))


main()