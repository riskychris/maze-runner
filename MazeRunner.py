import time
import copy
import heapq
import Mazes
from Mazes import Maze, Cell

infin = 1000000

class MazeRunner:

    def __init__(self, knownMaze, realMaze, backwards = False):
        self.knownMaze = knownMaze
        self.realMaze = realMaze

        if backwards:
            temp = knownMaze.target
            knownMaze.setTarget(knownMaze.start)
            knownMaze.setStart(temp)

        self.state = knownMaze.start
        self.target = knownMaze.target

        self.pointers = {}
        self.path = []

    def setCellH(self, cell, newH= None):
        self.knownMaze.setCellH(cell, newH)

    def setCellG(self, cell, g = None):
        if g == None:
            self.knownMaze.setCellG(cell, self.g(cell))
        else:
            self.knownMaze.setCellG(cell, g)

    def setCellSearch(self, cell, val):
        self.knownMaze.setCellSearch(cell, val)

    def g(self,cell):
        cell.g + 1

    def __str__(self):
        ret = "State: " + str(self.state) + "\n"
        ret += "Target: " + str(self.target) + "\n"
        ret += "Distance: " + str(self.knownMaze.h(self.state))+ "\n"
        return ret

    def expandNeighbor(self, neighbor, state):
        if neighbor.blocked:
            return False

        return neighbor.g > state.g + 1

    def breakTies(self, open):
        compare = []
        compare.append(heapq.heappop(open))
        max = 0

        while len(open) > 0 and open[0].g <= compare[0].g:
            compare.append(heapq.heappop(open))

        for i in range(len(compare)):
            if compare[i].g > compare[max].g:
                max = i

        winner = compare.pop(max)

        for loser in compare:
            heapq.heappush(open, loser)

        return winner, open


    def AStar(self, open, closed, counter, adaptive = False):
        while len(open) > 0 and self.target.g > open[0].g:
            state, open = self.breakTies(open)
            closed.append(state)

            for neighbor in self.knownMaze.getNeighbors(state):
                if neighbor in closed:
                    continue

                if neighbor.search < counter:
                    self.setCellG(neighbor, infin)
                    self.setCellSearch(neighbor, counter)


                #compare Costs
                if self.expandNeighbor(neighbor,state):
                    self.setCellG(neighbor, state.g + 1)
                    if neighbor.h == infin or not adaptive:
                        self.setCellH(neighbor)

                    if neighbor in open:
                        open.remove(neighbor)

                    self.pointers[neighbor.getKey()] = state
                    heapq.heappush(open, neighbor)

        return open, closed

    def repeatedAStar(self, i,  adaptive = False):
        counter = 0
        expanded = 0
        while not self.state == self.target:
            counter += 1
            self.state = self.knownMaze.start
            closed = []
            open = []

            self.setCellG(self.state, 0)
            self.setCellG(self.target, infin)
            self.setCellH(self.state)
            self.setCellSearch(self.state, counter)

            self.pointers = {}
            self.path = {}


            heapq.heappush(open, self.state)
            open, closed = self.AStar(open, closed, counter, adaptive)
            expanded += len(closed)

            if len(open) == 0:
                #self.realMaze.visualize()
                print("cannot reach goal")
                return expanded

            if adaptive:
                for cell in closed:
                    self.setCellH(cell, self.target.g - cell.g)


            step = self.target
            while step != self.state:
                self.path[self.pointers[step.getKey()].getKey()] = step
                step = self.pointers[step.getKey()]

            self.traverse(False)
        self.state = self.knownMaze.start
        self.traverse(True)
        self.realMaze.visualize(True, "./_CompletedMazes/Maze " + str(i) + ".png")


        return expanded


    def traverse(self, completed):
        while not self.state == self.target:
            self.observeNeighbors(self.state)
            nextState = self.path[self.state.getKey()]
            nextRow = nextState.row
            nextCol = nextState.col

            # next state is know observed by by the agent
            nextState = self.realMaze.cells[nextRow][nextCol]
            if nextState.blocked:
                return
            if completed:
                self.realMaze.setCellColor(self.state, 100)

            self.state = nextState


    def observeNeighbors(self, cell):
        neighbors = self.realMaze.getNeighbors(cell)

        for neighbor in neighbors:
            # sets this cell's blocked value to the real mazes blocked value
            self.knownMaze.cells[neighbor.row][neighbor.col].setBlocked(neighbor.blocked)

def runMaze(i, outputFile, backwards, adaptive):
    realMaze = Mazes.loadMaze(i)
    knownMaze = copy.deepcopy(realMaze)
    knownMaze.resetBlocked()

    agent = MazeRunner(knownMaze, realMaze, backwards)
    print(agent)

    #agent.realMaze.visualize()
    expanded = agent.repeatedAStar(i, adaptive)
    outputFile.write(str(expanded) + "\n")



def main():
    #Mazes.generateMazes(50)
    fname = "./CollectedData/Forward.txt"
    #outputFile = open(fname, "w+")
    #outputFile.close()
    for i in range(39, 50):
        outputFile = open(fname, "a+")
        runMaze(i, outputFile, False, False)
        outputFile.close()

main()