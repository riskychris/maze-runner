import random
import pickle
import matplotlib.pyplot as plt
from matplotlib import colors

infin = 1000000

# Cells make up the the grid of the Maze
class Cell:

    #Cell constructor
    def __init__(self, row, col):
        self.blocked = False
        self.visited = False
        self.row = row
        self.col = col
        self.h = infin
        self.g = infin
        self.search = 0

        #white
        self.color = 256

    def setColor(self,color):
        self.color = color

    def setBlocked(self, blocked):
        self.blocked = blocked
        if self.blocked:
            self.setG(infin)
            self.setColor(0)
        else:
            self.setColor(256)

    def setSearch(self,val):
        self.search = val

    def setVisited(self, visited):
        self.visited = visited

    def setH(self, h):
        self.h = h

    def setG(self, g):
        self.g = g

    def getKey(self):
        return "(" + str(self.row) + ", " + str(self.col)+ ")"
    # Cell comparators
    def gGreaterThan(self, cell):
        if self.g == infin:
            return True
        if cell.g == infin:
            return False

        return(self.g > cell.g)


    def __eq__(self, cell):
        #return false if not comparing to another cell
        if not isinstance(cell, Cell):
            return False

        # compare their (row, column) value
        return (self.row == cell.row and self.col == cell.col)

    def __lt__(self, cell):
        # return false if not comparing to another cell
        if not isinstance(cell, Cell):
            return False

        if self.g == infin:
            return False

        # -1 is infinity
        if cell.g == infin:
            return True

        # compare their g+h values
        return (self.g +self.h < cell.g + cell.h)


    def __le__(self, cell):
        # return false if not comparing to another cell
        if not isinstance(cell, Cell):
            return False

        if self.g == infin:
            return False

        # -1 is infinity
        if cell.g == infin:
            return True

        # compare their g+h values
        return (self.g +self.h <= cell.g + cell.h)

    def __gt__(self, cell):
        # return false if not comparing to another cell
        if not isinstance(cell, Cell):
            return False

        # -1 is infinity
        if self.g == infin:
            return True

        # compare their g+h values
        return (self.g +self.h > cell.g + cell.h)


    # Python's toString method
    def __str__(self):
        return "(" + str(self.row) + ", " + str(self.col) + \
               ", h=" + str(self.h)+ ", g=" + str(self.g) +", b=" +str(self.blocked)  +")"

# 2D array of cells that the agent will traverse
class Maze:

    # Maze Costructor
    def __init__(self, n):
        self.size = n
        self.cells = []
        self.start = None
        self.target = None

        for i in range(n):
            self.cells.append([])

            for j in range(n):
                self.cells[i].append(Cell(i, j))

    # Displays an image of the Maze
    def visualize(self, save = False, path = "Maze.png"):
        cells = []
        for row in range(len(self.cells)):
            cells.append([])
            for col in range(len(self.cells[row])):
                cells[row].append(self.cells[row][col].color)

        plt.imshow(cells,cmap = "hot")

        if save:
            plt.savefig(path)
        else:
            plt.show()


    # returns the  2D array of cells
    def getCells(self):
        cells = []
        for i in range(self.size):
            cells.append([])
            for j in range(self.size):
                cells[i].append(self.cells[i][j])
        return cells

    # set the color of the indicated cell
    def setCellColor(self,cell, color):
        self.cells[cell.row][cell.col].setColor(color)

    # set the heuristic of the indicated cell
    def setCellH(self, cell, newH = None):
        if newH is None:
            self.cells[cell.row][cell.col].setH(self.h(cell))
        else:
            self.cells[cell.row][cell.col].setH(newH)

    def h(self, cell):
        vert = abs(self.target.row - cell.row)
        hori = abs(self.target.col - cell.col)
        return vert + hori

    # set the cost of the indicated cell
    def setCellG(self, cell, g):
        self.cells[cell.row][cell.col].setG(g)

    def setCellSearch(self, cell, val):
        self.cells[cell.row][cell.col].setSearch(val)

    def setStart(self, start):
        self.start = start
        self.setCellG(start, 0)
        self.setCellH(start)
        self.setCellColor(start, 110)

    def setTarget(self, target):
        self.target = target
        self.setCellG(target, infin)
        self.setCellH(target)
        self.setCellColor(target, 150)

    # Returns array of the neighbors of cell
    def getNeighbors(self, cell):
        row = cell.row
        col = cell.col
        neighbors = []

        if row + 1 < self.size:
            neighbors.append(self.cells[row + 1][col])
        if row > 0:
            neighbors.append(self.cells[row - 1][col])
        if col + 1 < self.size:
            neighbors.append(self.cells[row][col + 1])
        if col + 1 > 0:
            neighbors.append(self.cells[row][col - 1])

        return neighbors

    # Returns array of unvisited neighbors of cell
    def getUnvisitedNeighbors(self, cell):
        neighbors = self.getNeighbors(cell)
        unvisited = []

        for neighbor in neighbors:
            if not neighbor.visited:
                unvisited.append(neighbor)

        return unvisited

    # Formatted printing of array
    def printCells(self):
        for i in range(self.size):
            print("Row " + str(i + 1) + ": ", end = '')

            if i < 99:
                print( " ", end = '')

            if i < 9:
                print( " ", end = '')

            for j in range(self.size):
                if self.cells[i][j].blocked:
                    print('2', end = '')
                else:
                    print(1, end = '')
            print()

    def printVisitedCells(self):
        for i in range(self.size):
            print("Row " + str(i + 1) + ": ", end='')

            if i < 99:
                print(" ", end='')

            if i < 9:
                print(" ", end='')

            for j in range(self.size):
                if self.cells[i][j].visited:
                    if self.cells[i][j].blocked:
                        print('2', end='')
                    else:
                        print(1, end='')
            print()


    def printUnvisitedCells(self):
        for i in range(self.size):
            print("Row " + str(i + 1) + ": ", end='')

            if i < 99:
                print(" ", end='')

            if i < 9:
                print(" ", end='')

            for j in range(self.size):
                if not self.cells[i][j].visited:
                    if self.cells[i][j].blocked:
                        print('2', end='')
                    else:
                        print(1, end='')
            print()

    # Sets all cells to unvisited. Called before saving a maze
    def resetVisited(self):
        for i in range(self.size):
            for j in range(self.size):
                self.cells[i][j].setVisited(False)

    def resetBlocked(self):
        for i in range(self.size):
            for j in range(self.size):
                self.cells[i][j].setBlocked(False)

def saveMaze(maze, i):
    maze.resetVisited()
    pickle.dump(maze, open("./_MazeData/Maze " + str(i) + ".p", "wb"))


def loadMaze(i):
    maze = pickle.load(open("./_MazeData/Maze " + str(i) + ".p", "rb"))
    return maze

# initializes a new maze and marks its cells as blocked of unblocked using DFS
def generate(n, j):
    maze = Maze(n)
    unvisited = maze.getCells()
    row = 0
    col = 0
    current = unvisited[row][col]
    backtrack = []

    while not current == []:
        row = current.row
        col = current.col

        if not current.visited:
            #True 30% of the time
            maze.cells[row][col].setBlocked(random.randrange(100) < 30)
            maze.cells[row][col].setVisited(True)

            for i in range(len(unvisited[row])):
                if unvisited[row][i].col == col:
                    unvisited[row].pop(i)
                    break

        neighbors = maze.getUnvisitedNeighbors(current)
        if len(neighbors) > 0:
            backtrack.append(current)
            current = neighbors[random.randrange(len(neighbors))]

        elif len(backtrack) > 0:
            current = backtrack.pop()

        else:
            current = []
            for i in range(len(unvisited)):
                if len(unvisited[i]) > 0:
                    current = unvisited[i][0]

    unvisited = maze.getCells()
    row = random.randrange(len(unvisited))
    col = random.randrange(len(unvisited[row]))

    # random unblocked target
    target = unvisited[row][col]
    while target.blocked:
        row = random.randrange(len(unvisited))
        col = random.randrange(len(unvisited[row]))
        target = unvisited[row][col]
    maze.setTarget(target)
    maze.setCellG(target, infin)
    maze.setCellH(target)



    # random unblocked start point
    start = unvisited[row][col]
    while start.blocked or start == target:
        row = random.randrange(len(unvisited))
        col = random.randrange(len(unvisited[row]))
        start = unvisited[row][col]
        maze.setCellH(start)

    maze.setCellG(start, 0)
    maze.setStart(start)

    maze.visualize(True, "./_InCompleteMazes/Maze " + str(j) + ".png")
    return maze

# generates m number of mazes
def generateMazes(m):
    for i in range(m):
        saveMaze(generate(101, i), i)

